<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \Artisan::call('migrate:refresh');

        $threads = factory('App\Thread', 50)->create();

        $threads->each(function($thread) {
           factory('App\Reply', 10)->create(['thread_id' => $thread->id]);
        });

        create('App\User', [
            'name' => 'Marty Sloan',
            'email' => 'martinsloan58@gmail.com',
            'password' => bcrypt('adminadmin')
        ]);
    }
}
